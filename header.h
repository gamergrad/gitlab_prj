#pragma once
#include <iostream>
#include <vector>
#include <string>

char get_char()
{
    while (true) 
    {
        char users_cin;
        
        std::cout << "Enter R to recover files with errors" << std::endl
                  << "Enter C to prevent system check" << std::endl 
                  << "Enter E to Exit\n>>";
        std::cin >> users_cin;
        std::cin.ignore(32767,'\n'); 
 
        if (users_cin == 'R' || users_cin == 'C' || users_cin == 'E')    
            return users_cin; 
        else 
            std::cout << "Input is invalid. Try again\n" << ">> ";
        } 
}
