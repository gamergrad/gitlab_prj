#include "header.h"

int main() {
    std::cout << "Hello world! This is gitLab project." << std::endl;
    std::vector<int> rand_ints;
    
    // New commit test. Loading screen.

    std::cout << "Loading...";
    for (size_t i = 0; i < 30; i++)
    {
        rand_ints.push_back(rand()%10);
        if(rand_ints[i]== 3)
            std::cout << "....";
    }
    std::cout << "Complete";

    // Work in a new branch "first_branch". Add error check
    std::cout << '\n' << "======== System check start===========" << std::endl;

    int all_files_count = 0;
    int file_correct_count = 0;
    int file_error_count =0;

    for(auto i : rand_ints){
        ++all_files_count;
        if(i%2){
            std::cout << "Error in File# " << all_files_count << std::endl;
            ++file_error_count;
        }
        else
            ++file_correct_count;
    }
    std::cout << "=========== Sytem check end ============="<< '\n' 
    << "Files with errors: " << file_error_count << '\n' 
    << "Correct files: "<< file_correct_count << std::endl; 

    //"first_branch" merged with "master". Add main menu and pull

    std::cout << '\t' << "FILE RECOVERY PROGRAM v.1.27" << '\n';
    
    char users_cin = 'A';
    bool is_filed_recovred = false;

    while(users_cin!= 'E'){

        users_cin = get_char();

        switch (users_cin)
        {
            case 'R':
                if(file_error_count>0){
                    std::cout << '\n' << "======= Files recovery start =======" << std::endl;
                        for (size_t i = 0; i < file_error_count; i++)
                            std::cout << "File recovered" << std::endl;
                    std::cout << "======= Files recovery ends =======" << std::endl;
                    file_error_count = 0;
                    is_filed_recovred = true;
                    break;
                }
                else {
                    std::cout << "\nNo errors find"<< std::endl;
                    break;
                }
            case 'C':
                std::cout << '\n' << "======== System check start===========" << std::endl;
                if(is_filed_recovred)
                    std::cout << "No errors" << std::endl;
                else
                    std::cout << "Files with errors: " << file_error_count << '\n' 
                              << "Correct files: "<< file_correct_count << std::endl; 
                    std::cout << "=========== Sytem check end ============="<< '\n';
                break;
            case 'E':
                return 0;
            default:
                std::cout << "Error in get_char function" << std::endl;
                break;
        }

    }

    // Create new branch "add_header". Add header.h and get_char()
    // Make git rebase "add_header" to master.
    // Add while for main menu. Finish project and remove all locals branches

    return 0;
}